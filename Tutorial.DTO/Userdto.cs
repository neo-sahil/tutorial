﻿namespace Tutorial.DTO
{
    public class Userdto
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public string UserCode { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string AuthKey { get; set; }
        public string Token { get; set; }
    }
}