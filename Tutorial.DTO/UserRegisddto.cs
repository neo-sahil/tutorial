﻿namespace Tutorial.DTO
{
    public class UserRegisddto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserCode { get; set; }
        public string UserPassword { get; set; }
        public long UserMobNo { get; set; }
        public string UserEmail { get; set; }
    }
}