﻿using Tutorial.DTO;
using Tutorial.Utils;

namespace Tutorial.Services.Infrastructure
{
    public interface IAuthServices
    {
        TResponce Login(Logindto logindto);
        TResponce UserReg(UserRegisddto userreg);
    }
}