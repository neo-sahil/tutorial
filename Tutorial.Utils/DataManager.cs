﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
namespace Tutorial.Utils
{
    public class DataManager
    {
        private readonly IConfiguration _configuration;

        public DataManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(_configuration.GetConnectionString("conn"));
        }

        public DataSet GetDataSet(string procedure, List<Parameter> par)
        {
            var ds = new DataSet();
            var cmd = new SqlCommand(procedure, GetConnection())
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = 0
            };
            foreach (var obj in par)
            {
                cmd.Parameters.AddWithValue("@" + obj.Name.Trim(), obj.Value);
            }
            var da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataTable GetDataTable(string query)
        {
            var dt = new DataTable();
            var cmd = new SqlCommand(query, GetConnection())
            {
                CommandTimeout = 0
            };
            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
    }
    
    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}