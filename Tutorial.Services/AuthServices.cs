﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Tutorial.DTO;
using Tutorial.Helper;
using Tutorial.Services.Infrastructure;
using Tutorial.Utils;

namespace Tutorial.Services
{
    public class AuthServices : IAuthServices
    {
        public AuthServices(IConfiguration configuration, IOptions<AppSetting> options)
        {
            _dataManger = new DataManager(configuration);
            _appSetting = options.Value;
        }

        #region ClassFields

        private readonly DataManager _dataManger;
        private DataSet _ds = new DataSet();
        private DataTable _dataTable = new DataTable();
        private List<Parameter> _lst = new List<Parameter>();
        private TResponce _responce = new TResponce();
        private readonly AppSetting _appSetting;
        #endregion


        public TResponce Login(Logindto logindto)
        {
            _dataTable =
                _dataManger.GetDataTable(
                    "select Usr_kid, Usr_Code, Usr_Passkey, Usr_Password from H_Usr where  Usr_Code = '" +
                    logindto.UserId + "'");
            if (_dataTable != null && _dataTable.Rows.Count > 0)
            {
                var kid = _dataTable.Rows[0]["Usr_kid"].ToString();
                var passkey = _dataTable.Rows[0]["Usr_Passkey"].ToString();
                var pass = _dataTable.Rows[0]["Usr_Password"].ToString();
                if (string.Equals(pass, Cryptography.Encrypt(logindto.UserPass, passkey)))
                {
                    _dataTable =
                        _dataManger.GetDataTable(
                            "select  Usr_Code, Usr_FirstName, Usr_Email, Usr_mobNo from H_Usr where Usr_kid = '" +
                            kid + "'");
                    var user = new Userdto();
                    if (_dataTable != null && _dataTable.Rows.Count > 0)
                    {
                        user.UserCode = _dataTable.Rows[0]["Usr_Code"].ToString();
                        user.Name = _dataTable.Rows[0]["Usr_FirstName"].ToString();
                        user.Email = _dataTable.Rows[0]["Usr_Email"].ToString();
                        user.MobileNo = _dataTable.Rows[0]["Usr_mobNo"].ToString();
                    }

                    user.AuthKey = Guid.NewGuid().ToString("N");
                    user.Token = GenerateToken(Convert.ToString(kid));
                    _responce.ResponceCode = (int) HttpStatusCode.OK;
                    _responce.ResponceStatus = true;
                    _responce.ResponceMessages = ResponceMessage.LoginSuccess;
                    _responce.ResponceData = user;
                }
                else
                {
                    _responce.ResponceCode = (int) HttpStatusCode.Unauthorized;
                    _responce.ResponceStatus = false;
                    _responce.ResponceMessages = ResponceMessage.InvalidCreadentials;
                }
            }
            else
            {
                _responce.ResponceCode = (int) HttpStatusCode.NotFound;
                _responce.ResponceStatus = false;
                _responce.ResponceMessages = ResponceMessage.UserNotFound;
            }

            return _responce;
        }

        public TResponce UserReg(UserRegisddto userreg)
        {
            var PassKey = Guid.NewGuid().ToString("n");
            var Pass = Cryptography.Encrypt(userreg.UserPassword, PassKey);
            _lst.Clear();
            _lst.Add(new Parameter(){Name = "flag",Value = 'I'});
            _lst.Add(new Parameter(){Name = "FirstName",Value = userreg.FirstName});
            _lst.Add(new Parameter(){Name = "LastName",Value = userreg.LastName});
            _lst.Add(new Parameter(){Name = "UsrCode",Value = userreg.UserCode});
            _lst.Add(new Parameter(){Name = "UsrPassKey",Value = PassKey});
            _lst.Add(new Parameter(){Name = "UsrPassword",Value = Pass});
            _lst.Add(new Parameter(){Name = "UsrMob",Value = userreg.UserMobNo.ToString()});
            _lst.Add(new Parameter() {Name = "UsrEmail", Value = userreg.UserEmail});

            _ds = _dataManger.GetDataSet("USP_User_Registration", _lst);
            if (_ds != null && _ds.Tables[0].Rows.Count > 0)
            {
                _responce.ResponceCode = (int) HttpStatusCode.OK;
                _responce.ResponceMessages = ResponceMessage.UserEntered;
                _responce.ResponceStatus = true;
                _responce.ResponceData = new
                {
                    FirstName = userreg.FirstName, LastName = userreg.LastName, Code = userreg.UserCode,
                    Mob = userreg.UserMobNo, Emial = userreg.UserEmail
                };
            }
            else
            {
                _responce.ResponceCode = (int) HttpStatusCode.BadRequest;
                _responce.ResponceMessages = ResponceMessage.UserNotEntered;
                _responce.ResponceStatus = false;
            }
            return _responce;
        }

        public string GenerateToken(string id)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            // var secBytes = Encoding.ASCII.GetBytes("Q8zBBFb3nU6yjMhsEfIsXQcrLh3eYHOUCeX_ZqHpS8TAg0xTZ650Pk2ZluarlJnAbAQ4Vh-q2v0EqPg36BJeR3jg2Mdh9o25UkuOFdeSNtaIi");

            var secBytes = Encoding.ASCII.GetBytes(_appSetting.Secret);
            var descriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", id) }),
                Expires = DateTime.Now.AddHours(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secBytes),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            return tokenHandler.WriteToken(tokenHandler.CreateToken(descriptor));

        }
    }
}