﻿using Microsoft.AspNetCore.Mvc;
using Tutorial.DTO;
using Tutorial.Services.Infrastructure;
using Tutorial.Utils;

namespace Tutorial.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthServices _authServices;

        public AuthController(IAuthServices authServices)
        {
            _authServices = authServices;
        }

        [HttpPost]
        public TResponce LogIn(Logindto login)
        {
            return _authServices.Login(login);
        }

        [HttpPost]
        public TResponce UsrReg(UserRegisddto userRegisddto)
        {
            return _authServices.UserReg(userRegisddto);
        } 
    }
}