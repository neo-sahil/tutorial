﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tutorial.Services;
using Tutorial.Services.Infrastructure;

namespace Tutorial.core
{
    public class BindDependency
    {
        public BindDependency(IServiceCollection services)
        {
            services.AddTransient<IAuthServices, AuthServices>();
        }
    }
}